# Prudential Control-M_V9
Ansible playbook to download the binaries and XML file of Control-M, untar and to install it. 

## Running Playbook
```
ansible-playbook -i inventorries/sgrtss -e "__host_group=rt-dev" install.yml
```

## Update inventory host_vars and group_vars before running the playbook
### set either one of the variables below in host_vars for primary/secondary/dr
```
__controlm_primary: true  
__controlm_secondary: true
__controlm_dr: true       
```
### set variables below in group_vars for nfs and host_interface
```
__nfs_server_ip
__nfs_server_share
__host_interface_name ... default to inventory_hostname_short
```
### inventory
```
inventories/sgrtss/hosts
[rt-dev]
sgrtss4m2k63s6p

[rt-uat]
sgrtssl4uikuby2

[rt-prod]
sgrtssdw2o6ebei
sgrtsszv3d1llb1
```

## Runnning Playbook from Jenkins
URL: https://jenkins-pss-jenkins-prd.sgapps-prod.pru.intranet.asia/job/RT-AI-ControlM-Server-Playbook/

Parameters:-

```
bucode: sgrtss
buenv: rt-dev
operation: install/uninstall
```
